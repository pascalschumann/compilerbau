package htw.compilerbau;

import htw.compilerbau.expressionInterpreter.Dfa;
import htw.compilerbau.expressionInterpreter.Lexer;
import htw.compilerbau.expressionInterpreter.Morpheme;
import htw.compilerbau.expressionInterpreter.Symbol;
import htw.compilerbau.expressionInterpreter.parsers.ExpressionParser;
import htw.compilerbau.expressionInterpreter.parsers.ExpressionSymbol;
import htw.compilerbau.pl0_compiler.FileUtils;
import htw.compilerbau.pl0_compiler.codegen.Compiler;
import htw.compilerbau.pl0_compiler.codegen.Utils;

import java.io.File;
import java.util.List;

public class Main {

    public static void main(final String[] args) {

        if (args.length != 1) {
            System.out.println("java -jar pl0_compiler.jar <fileName>");
        }
        final String testFile = args[0];

        System.out.println("Generating code for: " + testFile);
        final String source = FileUtils.readFileAsString(testFile);
        if (source.length() == 0) {
            System.out.println("Source should not be empty");
        }

        final Compiler compiler = new Compiler();
        final String actualGeneratedCode = compiler.generateCode(source);
        System.out.println("\n\n\n--------------------------------------------");
        if (actualGeneratedCode.isEmpty()) {
            System.out.println("Generated code should not be empty.");
        }

        // write file
        final byte[] bytes = Utils.hexStringToByteArray2(actualGeneratedCode);
        final String outputFile = new File(testFile).getName() + ".cl0";
        FileUtils.writeFileAsBinary(outputFile, bytes);

        System.out.println("Successfully generated code for: " + testFile);
        System.out.println("Output file is: ./" + outputFile);

    }

    private static void expressionInterpreter() {
        final String stringToLex = "5*7+2+8.0";
        System.out.println("To calculate: " + stringToLex);
        final Lexer lexer = new Lexer(stringToLex, new Dfa(new Symbol[]{
                new Symbol("^[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?", ExpressionSymbol.ZAHL),
                new Symbol("^\\+", ExpressionSymbol.PLUS),
                new Symbol("^\\*", ExpressionSymbol.MAL),
                new Symbol("^\\(", ExpressionSymbol.LINKE_KLAMMER),
                new Symbol("^\\)", ExpressionSymbol.RECHTE_KLAMMER)
        }));
        final List<Morpheme> morphemes = lexer.getMorphemes();
        if (morphemes.size() == 0) {
            System.out.println("Lexer could not lex that string: " + stringToLex);
            return;
        }
        // print
        System.out.println("Lexer found following morphemes:");
        for (final Morpheme morpheme : morphemes) {
            if (morpheme != null) {
                System.out.println(morpheme);
            }
        }

        new ExpressionParser(morphemes);
        if (morphemes.size() != 0) {
            System.out.println("Morphemes were NOT fully consumed: (wrong expression)");
            for (final Morpheme morpheme : morphemes) {

                System.out.println(morpheme);
            }
        } else {
            System.out.println("Morphemes were fully consumed");
        }

    }
}