package htw.compilerbau.expressionInterpreter;

public class Morpheme {
    private Symbol symbol;        /* Morphemcode */
    private int positionLine;/* Zeile       */
    private int positionColumn;    /* Spalte      */
    private String value;
    private int morphemeLength;   /* Morpheml„nge*/

    public Morpheme(final Symbol symbol, final int positionLine, final int positionColumn, final String value, final int morphemeLength) {
        this.symbol = symbol;
        this.positionLine = positionLine;
        this.positionColumn = positionColumn;
        this.value = value;
        this.morphemeLength = morphemeLength;
    }


    public Symbol getSymbol() {
        return symbol;
    }

    public void setSymbol(final Symbol symbol) {
        this.symbol = symbol;
    }

    public int getPositionLine() {
        return positionLine;
    }

    public void setPositionLine(final int positionLine) {
        this.positionLine = positionLine;
    }

    public int getPositionColumn() {
        return positionColumn;
    }

    public void setPositionColumn(final int positionColumn) {
        this.positionColumn = positionColumn;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    public int getMorphemeLength() {
        return morphemeLength;
    }

    public void setMorphemeLength(final int morphemeLength) {
        this.morphemeLength = morphemeLength;
    }

    @Override
    public String toString() {
        return symbol.getName().getAsString() + ": " + value;
    }
}
