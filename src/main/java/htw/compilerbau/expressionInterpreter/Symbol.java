package htw.compilerbau.expressionInterpreter;

import htw.compilerbau.expressionInterpreter.parsers.ExpressionSymbol;

import java.util.regex.Pattern;

public class Symbol {
    // TODO: this is not used, for what is morphemeCode
    private String morphemeCode;
    private Pattern pattern;
    private ExpressionSymbol name;


    public Symbol(final String pattern, final ExpressionSymbol name) {
        this.pattern = Pattern.compile(pattern);
        this.name = name;

    }

    public ExpressionSymbol getName() {
        return name;
    }

    public void setName(final ExpressionSymbol name) {
        this.name = name;
    }

    public String getMorphemeCode() {
        return morphemeCode;
    }

    public void setMorphemeCode(final String morphemeCode) {
        this.morphemeCode = morphemeCode;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public void setPattern(final Pattern pattern) {
        this.pattern = pattern;
    }

    @Override
    public String toString() {
        return name.getAsString() + ": " + pattern;
    }
}
