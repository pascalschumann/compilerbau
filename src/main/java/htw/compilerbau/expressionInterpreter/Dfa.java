package htw.compilerbau.expressionInterpreter;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;

public class Dfa {

    Symbol[] symbols = null;

    public Dfa(final Symbol[] symbols) {
        this.symbols = symbols;
    }

    public List<Morpheme> matchNext(final String stringToLex) {
        final List<Morpheme> morphemes = new LinkedList<>();
        final String[] lines = stringToLex.split("\n");
        for (int lineNumber = 0; lineNumber < lines.length; lineNumber++) {
            String line = lines[lineNumber];
            int columnPosition = 0;
            // Solange Zeile noch nicht vollständig konsumiert wurde
            while (line.length() > 0) {
                line = line.substring(columnPosition);
                boolean found = false;
                for (final Symbol symbol : symbols) {
                    try {

                        final Matcher matcher = symbol.getPattern().matcher(line);
                        found = matcher.find();
                        if (found) {
                            final int startPosition = matcher.start();
                            final String value = matcher.group();
                            morphemes.add(new Morpheme(symbol, lineNumber, startPosition, value, value.length()));
                            columnPosition = matcher.end();
                            break; // TODO: should not be needed, else multiple matchings, test at the at by removing this
                        }
                    } catch (final IndexOutOfBoundsException exception) {
                        System.out.println("Should not happen.");
                    }
                }
                if (!found && !line.isEmpty()) {
                    System.out.println("Could not lex following restOfString: " + line);
                    return new LinkedList<>();
                }
            }
        }
        return morphemes;
    }
}
