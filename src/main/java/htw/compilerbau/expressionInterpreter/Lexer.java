package htw.compilerbau.expressionInterpreter;

import java.util.LinkedList;
import java.util.List;

public class Lexer {

    String stringToLex = null;
    int position;
    List<Morpheme> morphemes = null;
    Dfa dfa = null;

    public Lexer(final String stringToLex, final Dfa dfa) {
        this.stringToLex = stringToLex;
        position = 0;
        morphemes = new LinkedList<>();
        this.dfa = dfa;
        morphemes = dfa.matchNext(stringToLex);

    }


    public List<Morpheme> getMorphemes() {
        return morphemes;
    }
}
