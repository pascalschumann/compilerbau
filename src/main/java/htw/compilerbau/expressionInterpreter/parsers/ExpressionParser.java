package htw.compilerbau.expressionInterpreter.parsers;

import htw.compilerbau.expressionInterpreter.Morpheme;

import java.util.List;

public class ExpressionParser {

    String[] terminals = {""};
    private final List<Morpheme> morphemes;

    public ExpressionParser(final List<Morpheme> morphemes) {
        this.morphemes = morphemes;
        // Startsymbol
        final double value = expression();
        System.out.println(value);
    }

    private Double expression() {
        return rightExpression(term());
    }

    private Double rightExpression(final double value) {
        if (morphemes.isEmpty()) {
            return value;
        }
        if (morphemes.get(0).getSymbol().getName().equals(ExpressionSymbol.PLUS)) {
            morphemes.remove(0);
            return rightExpression(value + term());
        } else {
            return value;
        }
    }

    private Double term() {
        return rightTerm(factor());
    }

    private Double rightTerm(final double value) {
        if (morphemes.isEmpty()) {
            return value;
        }
        if (morphemes.get(0).getSymbol().getName().equals(ExpressionSymbol.MAL)) {
            morphemes.remove(0);
            return value * rightTerm(factor());
        } else {
            return value;
        }

    }

    private Double factor() { // done
        if (morphemes.get(0).getSymbol().getName().equals(ExpressionSymbol.ZAHL)) {
            final String value = morphemes.get(0).getValue();
            morphemes.remove(0);
            return Double.parseDouble(value);

        } else if (morphemes.get(0).getSymbol().getName().equals(ExpressionSymbol.LINKE_KLAMMER)
                && morphemes.get(morphemes.size() - 1).getSymbol().getName().equals(ExpressionSymbol.RECHTE_KLAMMER)) {
            morphemes.remove(0);
            morphemes.remove(morphemes.size() - 1);
            return expression();

        }
        System.out.println("No correct morpheme recognized in factor()");
        return null;
    }
}
