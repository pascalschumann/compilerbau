package htw.compilerbau.expressionInterpreter.parsers;

public enum ExpressionSymbol {
    PLUS("PLUS"),
    MAL("MAL"),
    ZAHL("ZAHL"),
    LINKE_KLAMMER("LINKE_KLAMMER"),
    RECHTE_KLAMMER("RECHTE_KLAMMER");

    private String string = null;

    ExpressionSymbol(final String string) {
        this.string = string;
    }

    public String getAsString() {
        return string;
    }
}
