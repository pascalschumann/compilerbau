package htw.compilerbau.pl0_compiler.codegen;

import javax.xml.bind.DatatypeConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class Utils {
    /**
     * from: https://stackoverflow.com/a/140861
     * @param s
     * @return
     */
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    public static byte[] hexStringToByteArray2(String s) {
        return DatatypeConverter.parseHexBinary(s);
    }

    public static String byteArrayToHexString(byte[] b) {
        return DatatypeConverter.printHexBinary(b);
    }

    public static int littleEndianByteArrayToInt(byte[] b) {
        final ByteBuffer bb = ByteBuffer.wrap(b);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        return bb.getInt();
    }

    public static byte[] intToLittleEndianByteArray(int i) {
        final ByteBuffer bb = ByteBuffer.allocate(Integer.SIZE / Byte.SIZE);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        bb.putInt(i);
        return bb.array();
    }

    public static byte[] shortToLittleEndianByteArray(short i) {
        final ByteBuffer bb = ByteBuffer.allocate(Short.SIZE / Byte.SIZE);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        bb.putShort(i);
        return bb.array();
    }

    public static String toLittleEndianHexString(int i) {
        return byteArrayToHexString(intToLittleEndianByteArray(i));
    }

    public static String toLittleEndianHexString(short i) {
        return byteArrayToHexString(shortToLittleEndianByteArray(i));
    }
}
