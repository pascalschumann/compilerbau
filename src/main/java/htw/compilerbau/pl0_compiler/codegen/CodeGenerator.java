package htw.compilerbau.pl0_compiler.codegen;

import htw.compilerbau.pl0_compiler.codegen.semantic_methods.*;
import htw.compilerbau.pl0_compiler.lexer.Morpheme;
import htw.compilerbau.pl0_compiler.namensliste.ConstantBlock;
import htw.compilerbau.pl0_compiler.namensliste.MainNamensliste;
import htw.compilerbau.pl0_compiler.namensliste.Procedure;

/**
 * A wrapper that contains all code generating functions which can be accessed via objects of the
 * code-generating classes which contains the code-generating functions
 */
public class CodeGenerator {

    private String generatedCode = "";
    private final MainNamensliste mainNamensliste = new MainNamensliste();
    private Procedure currentProcedure = mainNamensliste.getMainProcedure();
    private Morpheme currentMorpheme;

    private final ConstantBlock constantBlock = new ConstantBlock();

    // hierarchy: program, block, statement, condition, expression, term, factor

    private final Block block = new Block(this);
    private final Condition condition = new Condition(this);
    private final Expression expression = new Expression(this);
    private final Factor factor = new Factor(this);
    private final Program program = new Program(this);
    private final Statement statement = new Statement(this);
    private final Term term = new Term(this);

    public Block getBlock() {
        return block;
    }

    public Condition getCondition() {
        return condition;
    }

    public Expression getExpression() {
        return expression;
    }

    public Factor getFactor() {
        return factor;
    }

    public Program getProgram() {
        return program;
    }

    public Statement getStatement() {
        return statement;
    }

    public Term getTerm() {
        return term;
    }

    public String getGeneratedCode() {
        return generatedCode;
    }

    public void setGeneratedCode(final String generatedCode) {
        this.generatedCode = generatedCode;
    }

    public MainNamensliste getMainNamensliste() {
        return mainNamensliste;
    }

    public Procedure getCurrentProcedure() {
        return currentProcedure;
    }

    public void setCurrentProcedure(final Procedure currentProcedure) {
        this.currentProcedure = currentProcedure;
    }

    public Morpheme getCurrentMorpheme() {
        return currentMorpheme;
    }

    public void setCurrentMorpheme(final Morpheme currentMorpheme) {
        this.currentMorpheme = currentMorpheme;
    }

    public ConstantBlock getConstantBlock() {
        return constantBlock;
    }
}
