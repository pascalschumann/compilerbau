package htw.compilerbau.pl0_compiler.codegen;

import htw.compilerbau.pl0_compiler.lexer.Morpheme;
import htw.compilerbau.pl0_compiler.parser.Parser;

import java.util.List;

public class Compiler {

    public String generateCode(String source){
        final Parser parser = new Parser();
        parser.parse(source);
        // TODO: NOTE: use hexStringToByteArray2 to put it to file and printHexBinary to read from byte[] again
        return parser.getGeneratedCode();
    }
}
