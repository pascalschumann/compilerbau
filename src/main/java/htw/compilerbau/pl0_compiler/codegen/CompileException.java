package htw.compilerbau.pl0_compiler.codegen;

public
class CompileException extends RuntimeException {

    /**
     * Constructs a {@code CompileException} with no detail message.
     */
    public CompileException() {
        super("Source code could not be compiled.");
    }

    /**
     * Constructs a {@code CompileException} with the specified
     * detail message.
     *
     * @param   s   the detail message.
     */
    public CompileException(String s) {
        super(s);
    }
}
