package htw.compilerbau.pl0_compiler.codegen.semantic_methods;

import htw.compilerbau.pl0_compiler.codegen.CodeGenerator;

public class Expression {

    private final CodeGenerator codeGenerator;

    public Expression(final CodeGenerator codeGenerator) {
        this.codeGenerator = codeGenerator;
    }

    /**
     * ex1 (negatives Vorzeichen)
     * - Codegenerierung vzMinus=0A
     */
    public void ex1() {
        final String generatedCode = codeGenerator.getCurrentProcedure().getGeneratedCode() + "0A";
        codeGenerator.getCurrentProcedure().setGeneratedCode(generatedCode);
    }

    /**
     * Codegenerierung opAdd
     */
    public void ex2() {
        final String generatedCode = codeGenerator.getCurrentProcedure().getGeneratedCode() + "0C";
        codeGenerator.getCurrentProcedure().setGeneratedCode(generatedCode);
    }

    /**
     * Codegenerierung opSub
     */
    public void ex3() {
        final String generatedCode = codeGenerator.getCurrentProcedure().getGeneratedCode() + "0D";
        codeGenerator.getCurrentProcedure().setGeneratedCode(generatedCode);
    }


}
