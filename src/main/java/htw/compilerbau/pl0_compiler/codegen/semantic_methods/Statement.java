package htw.compilerbau.pl0_compiler.codegen.semantic_methods;

import htw.compilerbau.pl0_compiler.codegen.CodeGenerator;
import htw.compilerbau.pl0_compiler.codegen.CompileException;
import htw.compilerbau.pl0_compiler.codegen.Utils;
import htw.compilerbau.pl0_compiler.namensliste.Label;
import htw.compilerbau.pl0_compiler.namensliste.Procedure;

/**
 * https://www.informatik.htw-dresden.de/~beck/Compiler/doc/CodegenStmt.html
 */
public class Statement {

    private final CodeGenerator codeGenerator;

    public Statement(final CodeGenerator codeGenerator) {
        this.codeGenerator = codeGenerator;
    }

    /**
     * Bezeichner gefunden	    Code	            Parameter
     * -----------------------------------------------------------
     * Local	                PushAdrVarLocal	    Relativadresse
     * Im Hauptprogramm	        PushAdrVarMain	    Relativadresse
     * In umgebender Prozedur	PushAdrVarGlobal	Relativadresse, Prozedurnummer
     */
    public void st1() {
        final String identifier = codeGenerator.getCurrentMorpheme().getValue().getString();
        if (codeGenerator.getCurrentProcedure().searchVariable(identifier) != null) {
            final String generatedCode =
                    codeGenerator.getCurrentProcedure().getGeneratedCode() + codeGenerator.getCurrentProcedure().generatePushAddressOfVariable(identifier);
            codeGenerator.getCurrentProcedure().setGeneratedCode(generatedCode);
        } else {
            throw new CompileException();
        }
    }

    /**
     * Im Stack steht nun die Adresse der Zielvariable und der Wert des fertig berechneten
     * Ausdrucks.
     * Codegenerierung : storval
     */
    public void st2() {
        /*final Variable lastCreatedVariable = codeGenerator.getCurrentProcedure().getLastCreatedVariable();
        lastCreatedVariable.setValue(codeGenerator.getCurrentMorpheme().getValue().getNumber());
        // workaround, TODO: fix this in lexer: 59 should be number not symbol
        if (lastCreatedVariable.getValue() == null) {
            lastCreatedVariable.setValue(codeGenerator.getCurrentMorpheme().getValue().getSymbol());
        }*/

        final String generatedCode =
                codeGenerator.getCurrentProcedure().getGeneratedCode() + "07";
        codeGenerator.getCurrentProcedure().setGeneratedCode(generatedCode);
    }

    /**
     * Generieren eines Labels, es zeigt auf den nächsten freien Speicherplatz des Codeausgabepuffers.
     * Codegenerierung jnot mit einer vorläufigen Relativadresse 0
     */
    public void st3() {
        final String generatedCode = codeGenerator.getCurrentProcedure().getGeneratedCode();
        codeGenerator.getCurrentProcedure().getLabelStack().push(generatedCode.length(),
                codeGenerator.getCurrentProcedure());
        final String jumpNot = "19" + "0000";
        codeGenerator.getCurrentProcedure().setGeneratedCode(generatedCode + jumpNot);
    }

    /**
     * Label auskellern
     * Relativadresse berechnen
     * Relativadresse in jmp-Befehl eintragen, dazu den Labelwert um 1 erhöhen, wenn das Label auf den jmp-Befehl zeigt.
     */
    public void st4() {
        final String generatedCode = codeGenerator.getCurrentProcedure().getGeneratedCode();
        final Label label = codeGenerator.getCurrentProcedure().getLabelStack().pop();
        final int lengthOfJnot = 6;
        final Short newRelativeAddress = (short) ((generatedCode.length() - (lengthOfJnot + label.getPositionInGeneratedCode())) / 2);
        final String prefix = generatedCode.substring(0, label.getPositionInGeneratedCode());
        final String newJumpNot = "19" + Utils.toLittleEndianHexString(newRelativeAddress);
        final String suffix = generatedCode.substring(label.getPositionInGeneratedCode() + lengthOfJnot);

        final String newGeneratedCode = prefix + newJumpNot + suffix;
        codeGenerator.getCurrentProcedure().setGeneratedCode(newGeneratedCode);

    }

    /**
     * Generieren eines Labels für Rücksprung am Schleifenende
     */
    public void st5() {
        final String generatedCode = codeGenerator.getCurrentProcedure().getGeneratedCode();
        codeGenerator.getCurrentProcedure().getLabelStack().push(generatedCode.length(),
                codeGenerator.getCurrentProcedure());
    }

    /**
     * Generieren eines Labels, es zeigt auf den nächsten freien Speicherplatz des
     * Codeausgabepuffers.
     * Codegenerierung jnot mit einer vorläufigen Relativadresse 0
     */
    public void st6() {
        st3();

    }

    /**
     * Labels auskellern
     * Relativadresse berechnen, so ähnlich, wie bei st4, jedoch müssen 3 Byte für den jmp-Befehl am
     * Ende der while-Anweisung freihalten werden (zu der berechneneten Relativadresse muss die
     * Länge des jmp-Befehls addiert werden addiert werden)
     * 2. Labelauskellern und jmp-Befehl generieren. Die Relativadresse muss so berechnet werden,dass der Sprung bei dem 1. Befehl von Condition landet.
     */
    public void st7() {
        String generatedCode = codeGenerator.getCurrentProcedure().getGeneratedCode();

        final Label label1 = codeGenerator.getCurrentProcedure().getLabelStack().pop();
        final int lengthOfJumpNot = 6;
        final int additionalSpaceForLastJump = 3;
        final Short newRelativeAddress = (short) ((generatedCode.length() - (lengthOfJumpNot +
                label1.getPositionInGeneratedCode())) / 2 + additionalSpaceForLastJump);
        final String prefix = generatedCode.substring(0, label1.getPositionInGeneratedCode());
        final String newJumpNot = "19" + Utils.toLittleEndianHexString(newRelativeAddress);
        final String suffix = generatedCode.substring(label1.getPositionInGeneratedCode() + lengthOfJumpNot);
        generatedCode = prefix + newJumpNot + suffix;

        final Label label2 = codeGenerator.getCurrentProcedure().getLabelStack().pop();
        final int lengthOfJump = 6;
        // negative address since we jump backwards: Two's complenet, negate all bits and add 1
        final Short relativeAddressSecondJump = (short) (-1 * ((generatedCode.length() + lengthOfJump
                - label2.getPositionInGeneratedCode()) / 2));
        final String jumpCommand = "18" + Utils.toLittleEndianHexString(relativeAddressSecondJump);
        codeGenerator.getCurrentProcedure().setGeneratedCode(generatedCode + jumpCommand);
    }

    /**
     * Prozeduraufruf
     * <p>
     * Bezeichner global suchen
     * nicht gefunden -> Fehlerbehandlung
     * gefunden -> ok, weiter.
     * Bezeichnet der Bezeichner eine Procedure?
     * Nein, eine Konstante oder Variable -> Fehlerbehandlung
     * ja -> ok, weiter
     * Codegenerierung call procedurenummer
     */
    public void st8() {
        final String identifier = codeGenerator.getCurrentMorpheme().getValue().getString();
        final Procedure foundProcedure = Procedure.searchProcedureGlobally(identifier, codeGenerator.getMainNamensliste().getMainProcedure());
        if (foundProcedure == null) {
            throw new CompileException();
        }
        final String generatedCode = codeGenerator.getCurrentProcedure().getGeneratedCode();
        final String callCommand = "16" + Utils.toLittleEndianHexString(foundProcedure.getProcedureIndex());
        codeGenerator.getCurrentProcedure().setGeneratedCode(generatedCode + callCommand);

    }

    /**
     * Bezeichner gfunden	    Code	            Parameter
     * Local	                PushAdrVarLocal	    Relativadresse
     * Im Hauptprogramm	        PushAdrVarMain	    Relativadresse
     * In umgebender Prozedur	PushAdrVarGlobal	Relativadresse, Prozedurnummer
     * <p>
     * Codegenerierung getval
     */
    public void st9() {
        final String identifier = codeGenerator.getCurrentMorpheme().getValue().getString();
        String generatedCode = codeGenerator.getCurrentProcedure().getGeneratedCode() +
                codeGenerator.getCurrentProcedure().generatePushAddressOfVariable(identifier);
        // getval
        generatedCode += "09";
        codeGenerator.getCurrentProcedure().setGeneratedCode(generatedCode);
    }

    /**
     * Codegenerierung putval
     */
    public void st10() {

        codeGenerator.getCurrentProcedure().setGeneratedCode(codeGenerator.getCurrentProcedure().getGeneratedCode() + "08");
    }


}
