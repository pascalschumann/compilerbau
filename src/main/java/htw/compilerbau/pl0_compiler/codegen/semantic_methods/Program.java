package htw.compilerbau.pl0_compiler.codegen.semantic_methods;

import htw.compilerbau.pl0_compiler.codegen.CodeGenerator;
import htw.compilerbau.pl0_compiler.codegen.Utils;
import htw.compilerbau.pl0_compiler.namensliste.Procedure;

/**
 * https://www.informatik.htw-dresden.de/~beck/Compiler/doc/CodegenProgram.html
 */
public class Program {

    private final CodeGenerator codeGenerator;

    public Program(final CodeGenerator codeGenerator) {
        this.codeGenerator = codeGenerator;
    }

    /**
     * - Schreiben der Anzahl der Prozeduren in das Codefile am Anfang
     * - entryproc mit Anzahl Tokens für Codelänge (short), ProcIndex start bei 1(short),
     * speicherplatzZuordnungsZähler Start bei 0(short)
     * (Codelänge in den Befehl entryProc als 1. Parameter nachtragen)
     * <p>
     * - Codegenerierung: retProc (Aufruf von Bl5)
     * - Schreiben des Konstantenblocks in das Codefile
     * <p>
     * - Code aus dem Codepuffer in die Ausgabedatei schreiben (anfügen)
     * <p>
     * (- Namensliste mit allen Konstanten-, Variablen- und Prozedurbeschreibungen auflösen; die
     * Prozedur selbst muss noch erhalten bleiben)
     */
    public void pr1() {
        // procedurenAnzahl
        final String numberOfProcedures = Utils.toLittleEndianHexString(codeGenerator.getMainNamensliste().getCurrentProcedureIndex() + 1);

        // Konstantenblock
        final String constantBlock = codeGenerator.getConstantBlock().toLittleEndianHexString();

        final String generatedCodeMain =
                generateEntryProcPlusGeneratedCode(codeGenerator.getMainNamensliste().getMainProcedure());
        final String subProcedures = codeGenerator.getGeneratedCode();

        codeGenerator.setGeneratedCode(numberOfProcedures + subProcedures + generatedCodeMain + constantBlock);
    }

    public static String generateEntryProcPlusGeneratedCode(final Procedure procedure) {
        String prefixToAdd = "";

        // entryproc:
        // codeLength
        final int lengthOfEntryProcAndRetProc = 8;
        prefixToAdd += "1A";
        // lencode
        final String lengthCode = Utils.toLittleEndianHexString((short) (procedure.getGeneratedCode().length() / 2
                + lengthOfEntryProcAndRetProc));
        prefixToAdd += lengthCode;
        // procIndex
        prefixToAdd += Utils.byteArrayToHexString(Utils.shortToLittleEndianByteArray(procedure.getProcedureIndex()));
        // speicherplatzZuordnungsZähler
        prefixToAdd += Utils.byteArrayToHexString(Utils.shortToLittleEndianByteArray(procedure.getSpeicherPlatzZuordnungsZaehler()));
        final String suffixToAdd = "17"; // retProc

        return prefixToAdd + procedure.getGeneratedCode() + suffixToAdd;
    }
}
