package htw.compilerbau.pl0_compiler.codegen.semantic_methods;

import htw.compilerbau.pl0_compiler.codegen.CodeGenerator;

public class Condition {

    private final CodeGenerator codeGenerator;
    private String lastOperator;

    public Condition(final CodeGenerator codeGenerator) {
        this.codeGenerator = codeGenerator;
    }

    /**
     * aka "co"
     * <p>
     * Codegenerierung odd
     */
    public void co1() {

        final String generatedCode = codeGenerator.getCurrentProcedure().getGeneratedCode();
        final String odd = "0B";
        codeGenerator.getCurrentProcedure().setGeneratedCode(generatedCode + odd);
    }

    /**
     * =
     */
    public void co2() {
        lastOperator = "10";
    }

    /**
     * #
     */
    public void co3() {
        lastOperator = "11";
    }

    /**
     * <
     */
    public void co4() {
        lastOperator = "12";
    }

    /**
     * <=
     */
    public void co5() {
        lastOperator = "13";
    }

    /**
     * >=
     */
    public void co6() {
        lastOperator = "14";
    }

    /**
     * >
     */
    public void co7() {
        lastOperator = "15";
    }

    /**
     * Codegenerierung des gespeicherten Vergleichsbefehles
     */
    public void co8() {
        final String generatedCode = codeGenerator.getCurrentProcedure().getGeneratedCode();
        codeGenerator.getCurrentProcedure().setGeneratedCode(generatedCode + lastOperator);
    }


}
