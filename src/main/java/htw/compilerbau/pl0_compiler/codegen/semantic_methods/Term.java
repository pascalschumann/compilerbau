package htw.compilerbau.pl0_compiler.codegen.semantic_methods;

import htw.compilerbau.pl0_compiler.codegen.CodeGenerator;

public class Term {

    private final CodeGenerator codeGenerator;

    public Term(final CodeGenerator codeGenerator) {
        this.codeGenerator = codeGenerator;
    }

    /**
     * Codegenerierung opMul
     */
    public void te1() {
        final String generatedCode = codeGenerator.getCurrentProcedure().getGeneratedCode() + "0E";
        codeGenerator.getCurrentProcedure().setGeneratedCode(generatedCode);
    }

    /**
     * Codegenerierung opDiv
     */
    public void te2() {
        final String generatedCode = codeGenerator.getCurrentProcedure().getGeneratedCode() + "0F";
        codeGenerator.getCurrentProcedure().setGeneratedCode(generatedCode);
    }

}
