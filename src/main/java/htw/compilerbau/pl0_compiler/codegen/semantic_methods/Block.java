package htw.compilerbau.pl0_compiler.codegen.semantic_methods;

import htw.compilerbau.pl0_compiler.codegen.CodeGenerator;
import htw.compilerbau.pl0_compiler.codegen.CompileException;
import htw.compilerbau.pl0_compiler.namensliste.Constant;
import htw.compilerbau.pl0_compiler.namensliste.Procedure;
import htw.compilerbau.pl0_compiler.namensliste.Variable;

/**
 * https://www.informatik.htw-dresden.de/~beck/Compiler/doc/CodegenBlock.html
 */
public class Block {

    private final CodeGenerator codeGenerator;

    public Block(final CodeGenerator codeGenerator) {
        this.codeGenerator = codeGenerator;
    }

    /**
     * bl1(Konstantenbezeichner):
     * lokale Suche nach dem Bezeichner
     * gefunden -> Fehlerbehandlung
     * nicht gefunden -> Bezeichner anlegen
     */
    public void bl1() {
        final String identifier = codeGenerator.getCurrentMorpheme().getValue().getString();
        if (identifier == null) {
            throw new CompileException();
        }
        final Constant constant =
                codeGenerator.getConstantBlock().searchConstantByIdentifier(identifier);
        if (constant != null) {
            throw new CompileException();
        }
        codeGenerator.getConstantBlock().createConstantWithIdentifier(identifier);
    }

    /**
     * bl2 (Konstantenwert):
     * Konstantenbeschreibung anlegen
     * Suche nach Konstante im Konstantenblock
     * gefunden -> Index der Konstanten eintragen in Konstantenbeschreibung
     * Konstante anlegen im Konstantenblock und Index der Konstanten eintragen in
     * Konstantenbeschreibung
     * In letzten Bezeichner Zeiger auf Konstante eintragen
     */
    public void bl2() {
        final Integer number = codeGenerator.getCurrentMorpheme().getValue().getNumber();
        if (number == null) {
            throw new CompileException();
        }
        final Constant lastCreatedConstant =
                codeGenerator.getConstantBlock().getLastCreatedConstant();
        lastCreatedConstant.setValue(number);
    }

    /**
     * - lokale Suche nach dem Bezeichner
     * gefunden -> Fehlerbehandlung
     * nicht gefunden -> Bezeichner anlegen
     * - Variablenbeschreibung anlegen und Pointer in Bezeichner eintragen
     * - Relativadresse ermitteln aus SpzzVar, SpzzVar um 4 erhöhen (Virtuelle Maschine arbeitet
     * mit 4 Byte langen long-Werten)
     */
    public void bl3() {
        final String identifier = codeGenerator.getCurrentMorpheme().getValue().getString();
        final Variable variable = codeGenerator.getCurrentProcedure().searchVariableLocally(identifier);
        if (variable == null) {
            codeGenerator.getCurrentProcedure().createVariable(identifier, null);
        } else {
            throw new CompileException();
        }
    }

    public void bl4() {
        final String identifier = codeGenerator.getCurrentMorpheme().getValue().getString();
        final Procedure procedure = codeGenerator.getCurrentProcedure().createProcedure(identifier);
        codeGenerator.setCurrentProcedure(procedure);
    }

    /**
     * Codegenerierung: retProcCode
     * <p>
     * Länge in den Befehl entryProc als 1. Parameter nachtragen
     * Code aus dem Codepuffer in die Ausgabedatei schreiben (anfügen)
     * Namensliste mit allen Konstanten-, Variablen- und Prozedurbeschreibungen auflösen; die
     * Prozedur selbst muss noch erhalten bleiben
     * Die Parent-Prozedur wird die aktuelle Prozedur
     */
    public void bl5() {

        final Procedure currentProcedure = codeGenerator.getCurrentProcedure();
        // add entryProc to generatedCode of procedure
        currentProcedure.setGeneratedCode(Program.generateEntryProcPlusGeneratedCode(currentProcedure));
        // add procedure generatedCode to global generated code
        codeGenerator.setGeneratedCode(codeGenerator.getGeneratedCode() + currentProcedure.getGeneratedCode());

        // parent procedure is now the currentProcedure
        codeGenerator.setCurrentProcedure(codeGenerator.getCurrentProcedure().getParentProcedure());

    }

    /**
     * Codeausgabepuffer initialisieren
     * Codegenerierung: entryProc(CodeLen, IdxProc, VarLen) mit CodeLen zunächst 0, IdxProc mitpCurrProc->Idx, und VarLen aus SpzzVar.
     */
    public void bl6() {

    }
}
