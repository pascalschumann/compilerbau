package htw.compilerbau.pl0_compiler.codegen.semantic_methods;

import htw.compilerbau.pl0_compiler.codegen.CodeGenerator;
import htw.compilerbau.pl0_compiler.codegen.CompileException;
import htw.compilerbau.pl0_compiler.namensliste.Constant;

public class Factor {

    private final CodeGenerator codeGenerator;

    public Factor(final CodeGenerator codeGenerator) {
        this.codeGenerator = codeGenerator;
    }

    /**
     * (Numeral)
     * - suchen der Konstante
     * - ggf. anlegen der Konstanten, wenn nicht gefunden
     * - Codegenerierung puConst (ConstIndex)
     */
    public void fa1() {
        Constant constant = codeGenerator.getConstantBlock().searchConstantByValue(
                codeGenerator.getCurrentMorpheme());
        if (constant == null) {
            constant = codeGenerator.getConstantBlock().createConstantWithValue(
                    codeGenerator.getCurrentMorpheme().getValue().getNumber());
        }

        codeGenerator.getCurrentProcedure().setGeneratedCode(
                codeGenerator.getCurrentProcedure().getGeneratedCode() + constant.generate());
    }

    /**
     * Bezeichner global suchen
     * nicht gefunden -> Fehlerbehandlung
     * gefunden -> ok.
     * Bezeichnet der Bezeichner eine Variable oder Konstante?
     * Nein, eine Prozedur -> Fehlerbehandlung
     * ja -> ok
     * <p>
     * Bezeichner gefunden 	   |Code	            |Parameter
     * ---------------------------------------------------------------
     * Local                   |PushValVarLocal     |Relativadresse
     * Im Hauptprogramm        |PushValVarMain      |Relativadresse
     * In umgebender Prozedur  |PushValVarGlobal    |Relativadresse, Prozedurnummer
     * Konstante               |PushConst           |ConstIndex
     */
    public void fa2() {
        final String identifier = codeGenerator.getCurrentMorpheme().getValue().getString();
        String generatedCode = "";
        final Constant constant =
                codeGenerator.getConstantBlock().searchConstantByIdentifier(identifier);

        if (constant != null) {
            generatedCode = constant.generate();
        }
        // a variable
        else {
            if (codeGenerator.getCurrentProcedure().searchVariable(identifier) != null) {
                generatedCode = codeGenerator.getCurrentProcedure().generatePushValueOfVariable(identifier);
            }
        }

        if (!generatedCode.isEmpty()) {
            codeGenerator.getCurrentProcedure().setGeneratedCode(
                    codeGenerator.getCurrentProcedure().getGeneratedCode() + generatedCode);
        } else {
            throw new CompileException();
        }

    }

}
