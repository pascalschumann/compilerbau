package htw.compilerbau.pl0_compiler.parser;

public enum RuleName {
    PROGRAM,
    BLOCK,
    EXPRESSION,
    TERM,
    STATEMENT,
    FACTOR,
    CONDITION;
}
