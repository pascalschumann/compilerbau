package htw.compilerbau.pl0_compiler.parser;

import htw.compilerbau.pl0_compiler.lexer.Morpheme;

import java.util.List;

public class ParseData {
    private List<Morpheme> morphemes;
    private List<Edge> traversedEdges;

    private ParseData() {

    }

    public ParseData(final List<Morpheme> morphemes, final List<Edge> traversedEdges) {
        this.morphemes = morphemes;
        this.traversedEdges = traversedEdges;
    }

    public List<Morpheme> getMorphemes() {
        return morphemes;
    }

    public void setMorphemes(final List<Morpheme> morphemes) {
        this.morphemes = morphemes;
    }

    public List<Edge> getTraversedEdges() {
        return traversedEdges;
    }

    public void setTraversedEdges(final List<Edge> traversedEdges) {
        this.traversedEdges = traversedEdges;
    }
}
