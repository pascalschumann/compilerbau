package htw.compilerbau.pl0_compiler.parser;

import htw.compilerbau.pl0_compiler.codegen.CodeGenerator;
import htw.compilerbau.pl0_compiler.lexer.Lexer;
import htw.compilerbau.pl0_compiler.lexer.Morpheme;
import htw.compilerbau.pl0_compiler.lexer.MorphemeCode;
import htw.compilerbau.pl0_compiler.lexer.symbols.Symbol;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Bauen Sie einen Parser für die Programmiersprache PL/0.
 * Implementieren Sie den Parser auf der Basis von Syntaxgraphen,
 * die jeweils als Array von Strukturen oder adäquat je nach gewählter Implementieruzngssprache zu repräsentieren sind.
 * Als Ergebnis sollte ein Akzeptor vorliegen, der die synatktische Richtigkeit eines PL/0-quelltextes erkennt.
 */
public class Parser {

    // rules
    Map<RuleName, Rule> rules = new HashMap<>();
    private final Rule condition = new Rule(RuleName.CONDITION);
    private final Rule factor = new Rule(RuleName.FACTOR);
    private final Rule statement = new Rule(RuleName.STATEMENT);
    private final Rule term = new Rule(RuleName.TERM);
    private final Rule expression = new Rule(RuleName.EXPRESSION);
    private final Rule block = new Rule(RuleName.BLOCK);
    private final Rule program = new Rule(RuleName.PROGRAM);

    // code generation
    final CodeGenerator codeGenerator = new CodeGenerator();

    // Condition
    private final Edge[] edgesCondition = new Edge[]{
            new Edge(0, 0),
            new Edge(0, 1, Symbol.ODD.getAsciiCode()),
            new Edge(1, 4, expression, codeGenerator.getCondition()::co1),

            // previously symbol array
            // new Edge(2, 3, new int[]{(int) '=', (int) '#', (int) '<', (int) '>',
            //        Symbol.LESS_EQUAL.getAsciiCode(), Symbol.GREATER_EQUAL.getAsciiCode()}),
            new Edge(2, 3, (int) '=', codeGenerator.getCondition()::co2),
            new Edge(2, 3, (int) '#', codeGenerator.getCondition()::co3),
            new Edge(2, 3, (int) '<', codeGenerator.getCondition()::co4),
            new Edge(2, 3, (int) '>', codeGenerator.getCondition()::co5),
            new Edge(2, 3, Symbol.LESS_EQUAL.getAsciiCode(), codeGenerator.getCondition()::co6),
            new Edge(2, 3, Symbol.GREATER_EQUAL.getAsciiCode(), codeGenerator.getCondition()::co7),
            // end "previously symbol array"
            new Edge(3, 4, expression, codeGenerator.getCondition()::co8),

            new Edge(0, 2, expression),
            new Edge(4, 5)
    };

    // Factor
    private final Edge[] edgesFactor = new Edge[]{
            new Edge(0, 0),
            new Edge(0, 3, MorphemeCode.MC_NUMBER, codeGenerator.getFactor()::fa1),
            new Edge(0, 1, (int) '('),

            new Edge(2, 3, (int) ')'),
            new Edge(0, 3, MorphemeCode.MC_IDENTIFIER, codeGenerator.getFactor()::fa2),

            new Edge(1, 2, expression),
            new Edge(3, 4)
    };

    // Statement
    private final Edge[] edgesStatement = new Edge[]{
            new Edge(0, 0),
            // ident
            new Edge(0, 1, MorphemeCode.MC_IDENTIFIER, codeGenerator.getStatement()::st1),
            new Edge(1, 2, Symbol.ERGIBT.getAsciiCode()),
            new Edge(2, 11, expression, codeGenerator.getStatement()::st2),
            // if
            new Edge(0, 3, Symbol.IF.getAsciiCode()),
            new Edge(3, 4, condition, codeGenerator.getStatement()::st3),
            new Edge(4, 5, Symbol.THEN.getAsciiCode()),
            new Edge(5, 11, statement, codeGenerator.getStatement()::st4),
            // while
            new Edge(0, 6, Symbol.WHILE.getAsciiCode(), codeGenerator.getStatement()::st5),
            new Edge(6, 7, condition, codeGenerator.getStatement()::st6),
            new Edge(7, 8, Symbol.DO.getAsciiCode()),
            new Edge(8, 11, statement, codeGenerator.getStatement()::st7),
            // begin
            new Edge(0, 9, Symbol.BEGIN.getAsciiCode()),
            new Edge(9, 10, statement),
            new Edge(10, 11, Symbol.END.getAsciiCode()),
            new Edge(10, 9, (int) ';'),
            // call
            new Edge(0, 12, Symbol.CALL.getAsciiCode()),
            new Edge(12, 11, MorphemeCode.MC_IDENTIFIER, codeGenerator.getStatement()::st8),
            // '?'
            new Edge(0, 13, (int) '?'),
            new Edge(13, 11, MorphemeCode.MC_IDENTIFIER, codeGenerator.getStatement()::st9),
            // '!'
            new Edge(0, 14, (int) '!'),
            new Edge(14, 11, expression, codeGenerator.getStatement()::st10),

            new Edge(11, 15)

            // new Edge(0, 11), // No statement is NOT allowed
    };

    // Term
    private final Edge[] edgesTerm = new Edge[]{
            new Edge(0, 0),

            new Edge(4, 1, factor, codeGenerator.getTerm()::te1),
            new Edge(2, 4, (int) '*'),

            new Edge(5, 1, factor, codeGenerator.getTerm()::te2),
            new Edge(2, 5, (int) '/'),

            new Edge(0, 1, factor),
            new Edge(1, 2),
            new Edge(2, 3),
    };


    // Expression
    private final Edge[] edgesExpression = new Edge[]{
            new Edge(0, 0),
            // Abweichung Codegenerierung: term nun 2x, führe Knoten 7 ein
            new Edge(0, 1, (int) '-'),
            new Edge(1, 2, term, codeGenerator.getExpression()::ex1),

            new Edge(3, 5, (int) '+'),
            new Edge(3, 6, (int) '-'),

            new Edge(7, 2, term),

            new Edge(5, 2, term, codeGenerator.getExpression()::ex2),

            new Edge(6, 2, term, codeGenerator.getExpression()::ex3),


            new Edge(0, 7),
            new Edge(2, 3),
            new Edge(3, 4),
    };


    // Block
    private final Edge[] edgesBlock = new Edge[]{
            new Edge(0, 0),
            new Edge(0, 1, Symbol.CONST.getAsciiCode()),
            new Edge(1, 2, MorphemeCode.MC_IDENTIFIER, codeGenerator.getBlock()::bl1),
            new Edge(2, 3, (int) '='),
            new Edge(3, 4, MorphemeCode.MC_NUMBER, codeGenerator.getBlock()::bl2),
            new Edge(4, 1, (int) ','),
            new Edge(4, 5, (int) ';'),
            new Edge(0, 5),

            new Edge(5, 6, Symbol.VAR.getAsciiCode()),
            new Edge(6, 7, MorphemeCode.MC_IDENTIFIER, codeGenerator.getBlock()::bl3),
            new Edge(7, 6, (int) ','),
            new Edge(7, 8, (int) ';'),
            new Edge(5, 8),

            new Edge(8, 9, Symbol.PROCEDURE.getAsciiCode()),
            new Edge(9, 10, MorphemeCode.MC_IDENTIFIER, codeGenerator.getBlock()::bl4),
            new Edge(10, 11, (int) ';'),

            new Edge(12, 8, (int) ';', codeGenerator.getBlock()::bl5),
            new Edge(8, 13, codeGenerator.getBlock()::bl6),

            new Edge(11, 12, block),
            new Edge(13, 14, statement),
            new Edge(14, 15)
    };

    // Program
    private final Edge[] edgesProgram = new Edge[]{
            new Edge(0, 0),
            new Edge(0, 1, block, null),
            new Edge(1, 2, (int) '.', codeGenerator.getProgram()::pr1),
            new Edge(2, 3)
    };

    public Parser() {
        // add the edges to the rules
        program.setEdges(edgesProgram);
        block.setEdges(edgesBlock);
        expression.setEdges(edgesExpression);
        term.setEdges(edgesTerm);
        statement.setEdges(edgesStatement);
        condition.setEdges(edgesCondition);
        factor.setEdges(edgesFactor);

        program.setAdjazenzList(buildAdjazenList(edgesProgram));
        block.setAdjazenzList(buildAdjazenList(edgesBlock));
        expression.setAdjazenzList(buildAdjazenList(edgesExpression));
        term.setAdjazenzList(buildAdjazenList(edgesTerm));
        statement.setAdjazenzList(buildAdjazenList(edgesStatement));
        condition.setAdjazenzList(buildAdjazenList(edgesCondition));
        factor.setAdjazenzList(buildAdjazenList(edgesFactor));

        // put rules to a hashmap
        rules.put(RuleName.PROGRAM, program);
        rules.put(RuleName.BLOCK, block);
        rules.put(RuleName.EXPRESSION, expression);
        rules.put(RuleName.TERM, term);
        rules.put(RuleName.STATEMENT, statement);
        rules.put(RuleName.CONDITION, condition);
        rules.put(RuleName.FACTOR, factor);

        for (final Rule rule : rules.values()) {
            for (final Edge edge : rule.getEdges()) {
                edge.setBelongsToRule(rule.getRuleName());
            }
        }

    }

    private Map<Integer, List<Edge>> buildAdjazenList(final Edge[] edges) {
        final Map<Integer, List<Edge>> adjazenzList = new HashMap<>();
        for (int i = 0; i < edges.length; i++) {
            final Edge edge = edges[i];
            if (adjazenzList.get(edge.getFrom()) == null) {
                adjazenzList.put(edge.getFrom(), new LinkedList<>());
            }
            adjazenzList.get(edge.getFrom()).add(edge);
        }
        return adjazenzList;
    }

    /**
     * @param program
     * @return empty list if parsing was successful
     */
    public List<Morpheme> parse(final String program) {
        final List<Morpheme> allTokens = Lexer.lexAllTokens(program);
        final List<Edge> traversedEdges = new LinkedList<>();
        while (!allTokens.isEmpty()) {
            final boolean successful = parseThis(new ParseData(allTokens, traversedEdges), null, null);
            if (!successful) {
                System.out.println("parseThis failed.");
                break;
            }
        }
        System.out.println();
        System.out.println("----------------------- END -----------------------------------");
        System.out.println("Traversed: " + traversedEdges);
        return allTokens;
    }

    private void printDebugParseThis(final ParseData parseData, final Edge currentEdge) {
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("MORPHEMES: " + parseData.getMorphemes());
        System.out.println("TRAVERSED: " + parseData.getTraversedEdges());
        System.out.println("parseThis 1: " + currentEdge);
    }

    private void printDebugWhile(final ParseData parseData, final Edge currentEdge) {
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("MORPHEMES: " + parseData.getMorphemes());
        System.out.println("TRAVERSED: " + parseData.getTraversedEdges());
        System.out.println("parseThis 2: " + currentEdge + " with morpheme: " + parseData.getMorphemes().get(0));
    }

    /**
     * @param currentRule or null if it is program
     * @param currentEdge or null if it is the first edge
     */
    private boolean parseThis(final ParseData parseData, Rule currentRule, Edge currentEdge) {
        printDebugParseThis(parseData, currentEdge);
        if (parseData.getMorphemes().isEmpty()) {
            return true;
        }
        if (currentRule == null) {
            currentRule = program;
        }
        if (currentEdge == null) {
            currentEdge = currentRule.getEdges()[0];
        }
        Edge oldEdge = null;

        while (true) {
            parseData.getTraversedEdges().add(currentEdge);
            // return if end of graph
            if (currentEdge.isNullEdge() && currentRule.getAdjazenzList().get(currentEdge.getTo()) == null) {
                System.out.println("End edge, end of rule: NULL edge");
                return true;
            }
            // return if morphemes are empty
            if (parseData.getMorphemes().isEmpty()) {
                return true;
            }
            // return false if no edge matched
            if (oldEdge != null && oldEdge.equals(currentEdge)) {
                System.out.println("No edge matched.");
                return false;
            }
            oldEdge = currentEdge;

            final Morpheme currentMorpheme = parseData.getMorphemes().get(0);

            printDebugWhile(parseData, currentEdge);

            // for debug purpose
            if (currentMorpheme.getValue() != null && currentMorpheme.getValue().getSymbol() != null && currentMorpheme.getValue().getSymbol() == 139) {
                System.out.println("THEN");
            }


            // iterate over all alternative edges to given edge
            for (int i = 0; i < currentRule.getAdjazenzList().get(currentEdge.getTo()).size(); i++) {

                final Edge loopEdge = currentRule.getAdjazenzList().get(currentEdge.getTo()).get(i);
                // backup ParseData to have backup for errorPath (backtracking)
                final List<Morpheme> clonedMorphemes = new LinkedList<>();
                final List<Edge> clonedTraversed = new LinkedList<>();
                clonedMorphemes.addAll(parseData.getMorphemes());
                clonedTraversed.addAll(parseData.getTraversedEdges());

                // skip startEdge
                if (currentEdge.getFrom() == 0 && currentEdge.getTo() == 0 && loopEdge.getFrom() == 0 && loopEdge.getTo() == 0) {
                    System.out.println("Loop: Skip, since its a start edge");
                    continue;
                }
                System.out.println("Loop: LOOP" + loopEdge);

                // cases: rule, morphemeCode, symbol
                if (loopEdge.getRule() != null) {
                    System.out.println("Case: rule");

                    final boolean successful = parseThis(parseData, loopEdge.getRule(), null);
                    if (successful) {
                        executeAction(loopEdge, parseData.getMorphemes().get(0));
                        currentEdge = loopEdge;
                        break;
                    } else {
                        System.out.println("Instead of rollback, return false.");
                        return false;
                    }


                } else if (loopEdge.getMorphemeCode() != null) {
                    System.out.println("Case: morphemeCode");
                    if (currentMorpheme.getMorphemeCode().equals(loopEdge.getMorphemeCode())) {

                        executeAction(loopEdge, parseData.getMorphemes().get(0));
                        parseData.getMorphemes().remove(0);
                        currentEdge = loopEdge;
                        break;

                    }
                } else if (loopEdge.getSymbol() != null) {
                    System.out.println("Case: symbol");
                    if (currentMorpheme.getValue().getSymbol() != null && currentMorpheme.getValue().getSymbol().equals(loopEdge.getSymbol())) {

                        executeAction(loopEdge, parseData.getMorphemes().get(0));
                        parseData.getMorphemes().remove(0);
                        currentEdge = loopEdge;
                        break;

                    }
                } else if (loopEdge.isNullEdge()) {
                    System.out.println("Case: NULL edge");
                    currentEdge = loopEdge;
                    break;

                } else {
                    System.out.println("END: false, NO null edge, leave parseThis with false.");
                    System.out.println("------------------------------------------------------------");
                    return false;
                }
            }

        }

    }

    private void executeAction(final Edge loopEdge, final Morpheme morpheme) {
        codeGenerator.setCurrentMorpheme(morpheme);
        if (loopEdge.getAction() != null) {
            System.out.println("Execute action of edge!");
            loopEdge.getAction().executeThis();
        }
    }

    private void rollback(final ParseData parseData, final List<Morpheme> clonedMorphemes, final List<Edge> clonedTraversed) {
        // rollback changes
        System.out.println("rollback");
        parseData.setMorphemes(clonedMorphemes);
        parseData.setTraversedEdges(clonedTraversed);
    }

    public String getGeneratedCode() {
        return codeGenerator.getGeneratedCode();
    }
}

