package htw.compilerbau.pl0_compiler.parser;

import htw.compilerbau.pl0_compiler.lexer.MorphemeCode;

public class Edge {

    private int from;
    private int to;
    private Integer symbol = null;
    private Rule rule = null;
    private MorphemeCode morphemeCode = null;
    private RuleName belongsToRule = null;
    // Can always be null, then no action will be executed, once the edge passes in parser
    private Action action = null;

    private Edge() {

    }

    /**
     * For Nil edges
     */
    public Edge(final int from, final int to) {
        this.from = from;
        this.to = to;
    }

    /**
     * For Nil edges
     */
    public Edge(final int from, final int to, final Action action) {
        this.from = from;
        this.to = to;
        this.action = action;
    }

    public Edge(final int from, final int to, final Integer symbol) {
        this.from = from;
        this.to = to;
        this.symbol = symbol;
    }

    public Edge(final int from, final int to, final Integer symbol, final Action action) {
        this.from = from;
        this.to = to;
        this.symbol = symbol;
        this.action = action;
    }

    public Edge(final int from, final int to, final MorphemeCode morphemeCode, final Action action) {
        this.from = from;
        this.to = to;
        this.morphemeCode = morphemeCode;
        this.action = action;
    }

    public Edge(final int from, final int to, final Rule rule) {
        this.from = from;
        this.to = to;
        this.rule = rule;
    }

    public Edge(final int from, final int to, final Rule rule, final Action action) {
        this.from = from;
        this.to = to;
        this.rule = rule;
        this.action = action;
    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }

    public Integer getSymbol() {
        return symbol;
    }

    public Rule getRule() {
        return rule;
    }

    public MorphemeCode getMorphemeCode() {
        return morphemeCode;
    }

    @Override
    public String toString() {
        final String beginString = "EDGE \"" + this.getBelongsToRule() + ": " + Integer.toString(from) + "-->" + Integer.toString(to) + ": (";
        final String endString = ")\"";
        if (rule != null) {
            return beginString +
                    rule.getRuleName() + endString;
        }
        if (symbol != null && symbol < 128) {
            return beginString +
                    (char) symbol.intValue() + endString;
        }
        if (symbol != null && symbol >= 128) {
            for (final htw.compilerbau.pl0_compiler.lexer.symbols.Symbol enumSymbol : htw.compilerbau.pl0_compiler.lexer.symbols.Symbol.values()) {
                if (enumSymbol.getAsciiCode() == symbol) {
                    return beginString +
                            enumSymbol.getString() + endString;
                }
            }

        }
        return beginString +
                "Null edge" + endString;

    }

    public boolean isNullEdge() {
        return symbol == null && this.rule == null && this.morphemeCode == null;
    }

    public RuleName getBelongsToRule() {
        return belongsToRule;
    }

    public void setBelongsToRule(final RuleName belongsToRule) {
        this.belongsToRule = belongsToRule;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(final Action action) {
        this.action = action;
    }
}
