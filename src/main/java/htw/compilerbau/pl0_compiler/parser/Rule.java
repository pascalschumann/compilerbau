package htw.compilerbau.pl0_compiler.parser;

import java.util.List;
import java.util.Map;

public class Rule {
    private Edge[] edges = null;
    Map<Integer, List<Edge>> adjazenzList = null;
    private RuleName ruleName = null;

    Rule(final RuleName ruleName) {
        this.edges = edges;
        this.ruleName = ruleName;
    }

    private Rule() {

    }

    public Edge[] getEdges() {
        return edges;
    }

    public RuleName getRuleName() {
        return ruleName;
    }

    public void setEdges(final Edge[] edges) {
        this.edges = edges;
    }

    public Map<Integer, List<Edge>> getAdjazenzList() {
        return adjazenzList;
    }

    public void setAdjazenzList(final Map<Integer, List<Edge>> adjazenzList) {
        this.adjazenzList = adjazenzList;
    }

    @Override
    public String toString() {
        return ruleName.toString();
    }
}
