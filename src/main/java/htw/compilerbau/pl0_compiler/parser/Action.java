package htw.compilerbau.pl0_compiler.parser;

/**
 * to avoid reflection we give as parameter the implemented interface Action.
 * Its only containing function will be executed the edge is passing in parser.
 */
public interface Action {
    void executeThis();
}
