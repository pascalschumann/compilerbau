package htw.compilerbau.pl0_compiler.lexer.symbols;

public enum Symbol {

    // zNIL,
    //  zERG=128,zLE,zGE,
    //  zBGN,zCLL,zCST,zDO,zEND,zIF,zODD,zPRC,zTHN,zVAR,zWHL
    NIL("", 0),
    ERGIBT(":=", 128),
    LESS_EQUAL("<=", 129),
    GREATER_EQUAL(">=", 130),
    BEGIN("BEGIN", 131),
    CALL("CALL", 132),
    CONST("CONST", 133),
    DO("DO", 134),
    END("END", 135),
    IF("IF", 136),
    ODD("ODD", 137),
    PROCEDURE("PROCEDURE", 138),
    THEN("THEN", 139),
    VAR("VAR", 140),
    WHILE("WHILE", 141);

    private final String string;
    private final int asciiCode;

    Symbol(final String string, final int asciiCode) {
        this.string = string;
        this.asciiCode = asciiCode;
    }

    public String getString() {
        return string;
    }

    public int getAsciiCode() {
        return asciiCode;
    }

}
