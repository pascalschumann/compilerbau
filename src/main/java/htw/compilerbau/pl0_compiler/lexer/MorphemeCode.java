package htw.compilerbau.pl0_compiler.lexer;

public enum MorphemeCode {
    MC_EMPTY,
    MC_SYMBOL,
    MC_NUMBER,
    MC_IDENTIFIER,
    MC_STRING;

    MorphemeCode() {

    }
}
