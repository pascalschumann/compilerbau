package htw.compilerbau.pl0_compiler.lexer;

public class Morpheme {
    private MorphemeCode morphemeCode;        /* Morphemcode */
    private int positionLine;/* Zeile       */
    private int positionColumn;    /* Spalte      */
    private Value value;
    private int morphemeLength;   /* Morpheml„nge*/

    public Morpheme(final MorphemeCode morphemeCode, final int positionLine, final int positionColumn, final Value value, final int length) {
        this.morphemeCode = morphemeCode;
        this.positionLine = positionLine;
        this.positionColumn = positionColumn;
        this.value = value;
        this.morphemeLength = length;
    }


    public MorphemeCode getMorphemeCode() {
        return morphemeCode;
    }

    public void setMorphemeCode(final MorphemeCode morphemeCode) {
        this.morphemeCode = morphemeCode;
    }

    public int getPositionLine() {
        return positionLine;
    }

    public void setPositionLine(final int positionLine) {
        this.positionLine = positionLine;
    }

    public int getPositionColumn() {
        return positionColumn;
    }

    public void setPositionColumn(final int positionColumn) {
        this.positionColumn = positionColumn;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(final Value value) {
        this.value = value;
    }

    public int getMorphemeLength() {
        return morphemeLength;
    }

    public void setMorphemeLength(final int morphemeLength) {
        this.morphemeLength = morphemeLength;
    }


    @Override
    public String toString() {
        // return "\"" + morphemeCode + ":'" + value.toString() + "'" + "\"";
        return "\"" + morphemeCode + ": '" + value.toString() + "'" + "\"";
    }
}
