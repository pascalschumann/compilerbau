package htw.compilerbau.pl0_compiler.lexer;

import htw.compilerbau.pl0_compiler.lexer.symbols.Symbol;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Lexer {

    private final String[] ROW_STATES = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8"};
    private final CharClass[] COLUMN_CHAR_CLASSES = new CharClass[]{CharClass.SPECIAL, CharClass.DIGIT,
            CharClass.ALPHA, CharClass.COLON, CharClass.EQUAL_SIGN, CharClass.SMALLER, CharClass.GREATER, CharClass.SONSTIGE};
    private final StateMaschine stateMaschine;

    private final Map<String, String> keywordMap = new HashMap<>();

    private String program = null;
    private String programRest = null;
    // private Morpheme lastMorpheme = null; TODO: this must used as soon as lexer is stable
    private Morpheme lastMorpheme = null;
    private String currentString = null;
    private String currentState = null;
    private String currentCharacter = null;
    private int currentPositionLine = 0;/* Zeile       */
    private final int currentPositionColumn = 0;    /* Spalte      */
    private boolean done = false;
    final String startState = "0";


    String[][] cells = {
            // 	SoZei	Ziffer	Buchstabe	:	=	<	>	Sonst
            {"s+l+b", "2 s+l", "1 g+l", "3 s+l", "s+l+b", "4 s+l", "5 s+l", "l"},
            {"b", "1 s+l", "1 g+l", "b", "b", "b", "b", "b"},
            {"b", "2 s+l", "b", "b", "b", "b", "b", "b"},
            {"b", "b", "b", "b", "6 s+l ", "b", "b", "b"},
            {"b", "b", "b", "b", "7 s+l ", "b", "b", "b"},
            {"b", "b", "b", "b", "8 s+l ", "b", "b", "b"},
            {"b", "b", "b", "b", "b", "b", "b", "b"},
            {"b", "b", "b", "b", "b", "b", "b", "b"},
            {"b", "b", "b", "b", "b", "b", "b", "b"}
    };

    /**
     * init the lexer
     *
     * @param program source code as string
     */
    public Lexer(final String program) {
        this.program = program;
        final HashMap<String, HashMap<CharClass, StateTableCell>> stateTable = new HashMap<>();
        // for (final String possibleState : ROW_STATES) {
        for (int i = 0; i < ROW_STATES.length; i++) {
            final HashMap<CharClass, StateTableCell> row = new HashMap<>();
            // for (final CharClass charClass : CharClass.values()) {
            for (int j = 0; j < COLUMN_CHAR_CLASSES.length; j++) {

                String nextState = null;
                String actions = null;
                final String cell = cells[i][j];
                final String[] splittedCell = cell.split(" ");
                if (splittedCell.length <= 1) {
                    // nextState stays null
                    actions = cell;
                } else {
                    nextState = splittedCell[0];
                    actions = splittedCell[1];
                }
                row.put(COLUMN_CHAR_CLASSES[j], new StateTableCell(nextState, actions));
            }
            stateTable.put(ROW_STATES[i], row);
        }
        stateMaschine = new StateMaschine(stateTable);

        // init keyword hashmap
        for (final Symbol symbol : Symbol.values()) {
            keywordMap.put(symbol.getString(), symbol.getString());
        }

        initLexer();
    }

    private void initLexer() {
        // init
        if (programRest == null) {
            programRest = program;
        }
        if (programRest.length() == 0) {
            lastMorpheme = null;
            return;
        }
        lastMorpheme = null;
        currentState = startState;
        currentString = "";
        done = false;
        // initial reading
        fl();
    }

    /**
     * lesen
     */
    public void fl() {
        // skip reading if there is no character left
        if (programRest.length() == 0) {
            currentCharacter = "";
            return;
        }
        final String newCharacter = programRest.substring(0, 1);
        // überlesen des Absatzes
        if (newCharacter.equals("\n")) {
            currentPositionLine++;

        }
        currentCharacter = newCharacter;
        programRest = programRest.substring(1);
    }

    /**
     * beenden
     */
    public void fb() {

        switch (currentState) {
            // SOnderzeichen
            case "0": {
                if (currentString.equals("")) {
                    Value value = new Value();
                    value.setString("");
                    lastMorpheme = new Morpheme(MorphemeCode.MC_EMPTY, currentPositionLine,
                            currentPositionColumn, value, currentString.length());
                } else {
                    Value value = new Value();
                    value.setSymbol((int) currentString.charAt(0));
                    lastMorpheme = new Morpheme(MorphemeCode.MC_SYMBOL, currentPositionLine,
                            currentPositionColumn, value, currentString.length());
                }
            }
            break;
            /* Zahl */
            case "2": {
                Value value = new Value();
                value.setNumber(Integer.parseInt(currentString));
                lastMorpheme = new Morpheme(MorphemeCode.MC_NUMBER, currentPositionLine,
                        currentPositionColumn, value, currentString.length());
            }
            break;
            // Schlüsselwort aka Symbol or Bezeichner
            case "1": {
                // key word from language
                if (keywordMap.containsKey(currentString)) {
                    Value value = new Value();
                    value.setSymbol(Symbol.valueOf(currentString.toUpperCase()).getAsciiCode());
                    lastMorpheme = new Morpheme(MorphemeCode.MC_SYMBOL, currentPositionLine,
                            currentPositionColumn, value, currentString.length());
                }
                // identifier
                else {
                    lastMorpheme = new Morpheme(MorphemeCode.MC_IDENTIFIER, currentPositionLine,
                            currentPositionColumn, new Value(currentString), currentString.length());
                }
            }
            ;
            break;
            // ":"
            case "3": {
                Value value = new Value();
                value.setSymbol((int) ':');
                lastMorpheme = new Morpheme(MorphemeCode.MC_SYMBOL, currentPositionLine,
                        currentPositionColumn, value, currentString.length());
            }
            break;
            // "<"
            case "4": {
                Value value = new Value();
                value.setSymbol((int) '<');
                lastMorpheme = new Morpheme(MorphemeCode.MC_SYMBOL, currentPositionLine,
                        currentPositionColumn, value, currentString.length());
            }
            break;
            // ">"
            case "5": {
                Value value = new Value();
                value.setSymbol((int) '>');
                lastMorpheme = new Morpheme(MorphemeCode.MC_SYMBOL, currentPositionLine,
                        currentPositionColumn, value, currentString.length());
            }
            break;
            /* Ergibtzeichen */
            case "6": {
                Value value = new Value();
                value.setSymbol(Symbol.ERGIBT.getAsciiCode());
                lastMorpheme = new Morpheme(MorphemeCode.MC_SYMBOL, currentPositionLine,
                        currentPositionColumn, value, currentString.length());
            }
            break;
            /*  KleinerGleich  */
            case "7": {
                Value value = new Value();
                value.setSymbol(Symbol.LESS_EQUAL.getAsciiCode());
                lastMorpheme = new Morpheme(MorphemeCode.MC_SYMBOL, currentPositionLine,
                        currentPositionColumn, value, currentString.length());
            }
            break;
            /*  GroesserGleich  */
            case "8": {
                Value value = new Value();
                value.setSymbol(Symbol.GREATER_EQUAL.getAsciiCode());
                lastMorpheme = new Morpheme(MorphemeCode.MC_SYMBOL, currentPositionLine,
                        currentPositionColumn, value, currentString.length());
            }
            break;


        }
        done = true;
        currentString = "";

    }

    /**
     * groß schreiben und lesen
     */
    public void fgl() {
        fg();
        fl();
    }

    /**
     * schreiben und lesen
     */
    public void fsl() {
        fs();
        fl();
    }

    /**
     * schreiben
     */
    public void fs() {
        currentString += currentCharacter;
    }

    /**
     * schreiben
     */
    public void fg() {
        currentString += currentCharacter.toUpperCase();
    }

    /**
     * schreiben, lesen, beenden
     */
    public void fslb() {
        fsl();
        fb();
    }


    /**
     * gives next morpheme
     *
     * @return next morpheme or null if no one comes
     */
    public void lex() {

        // init

        // break condition for lexer
        // programRest.length() == 0 && currentCharacter.equals("")
        try {
            if (lastMorpheme.getValue().symbol.equals(46)) {
                lastMorpheme = null;
                return;
            }
        }
        catch(NullPointerException e){
            // pass, since break condition could not be fulfilled
        }
        // reset state
        lastMorpheme = null;
        currentState = startState;
        done = false;


        while (!done) {
            final StateTableCell cell = stateMaschine.readCell(currentState, currentCharacter);
            if (cell.getNextState() != null) {
                currentState = cell.getNextState();
            }
            for (final String action : cell.getActions()) {
                Method method = null;
                try {
                    method = this.getClass().getMethod("f" + action);
                } catch (final SecurityException e) {
                    e.printStackTrace();
                } catch (final NoSuchMethodException e) {
                    e.printStackTrace();
                }

                try {
                    method.invoke(this);
                } catch (final IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (final IllegalAccessException e) {
                    e.printStackTrace();
                } catch (final InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Morpheme getLastMorpheme() {
        return lastMorpheme;
    }

    public Morpheme getNextMorpheme() {
        lex();
        return lastMorpheme;
    }

    public static List<Morpheme> lexAllTokens(final String program) {

        final List<Morpheme> allTokens = new LinkedList<>();
        final Lexer lexer = new Lexer(program);

        Morpheme morpheme = lexer.getNextMorpheme();
        allTokens.add(morpheme);
        int i = 0;
        while (lexer.getLastMorpheme() != null) {
            i++;
            morpheme = lexer.getNextMorpheme();
            if (morpheme != null && !morpheme.getMorphemeCode().equals(MorphemeCode.MC_EMPTY)) {
                allTokens.add(morpheme);
            }
        }
        return allTokens;
    }
}
