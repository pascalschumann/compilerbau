package htw.compilerbau.pl0_compiler.lexer;

import htw.compilerbau.pl0_compiler.lexer.symbols.Symbol;

/**
 * reflects the ugly union which can be take only one of these members
 */
public class Value {
    Integer number = null;
    String string = null;
    Integer symbol = null;

    /*
    nicht eindeutig !!!
    public Value(final Integer number) {
        this.number = number;
    }


    public Value(final int symbol) {
        this.symbol = symbol;
    }*/

    public Value(){

    }

    public Value(final String string) {
        this.string = string;
    }

    public Integer getNumber() {
        return number;
    }

    public String getString() {
        return string;
    }

    public Integer getSymbol() {
        return symbol;
    }

    public void setNumber(final Integer number) {
        this.number = number;
    }

    public void setString(final String string) {
        this.string = string;
    }

    public void setSymbol(final Integer symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        if (number != null) {
            return number.toString();
        } else if (string != null) {
            return string;
        } else if (symbol != null) {
            for (final Symbol symbol : Symbol.values()) {
                if (symbol.getAsciiCode() == this.symbol.intValue()) {
                    return symbol.getString();
                }
            }
            return Character.toString((char) symbol.intValue());
        }
        return "VALUE was NULL.";
    }


}
