package htw.compilerbau.pl0_compiler.lexer;

import java.util.HashMap;

public class StateMaschine {

    private final HashMap<String, HashMap<CharClass, StateTableCell>> stateTable;

    StateMaschine(final HashMap<String, HashMap<CharClass, StateTableCell>> stateTable) {
        this.stateTable = stateTable;
    }

    public StateTableCell readCell(final String currentState, final String oneChar) {
        return stateTable.get(currentState).get(CharClass.getCharClass(oneChar.toUpperCase()));
    }

}
