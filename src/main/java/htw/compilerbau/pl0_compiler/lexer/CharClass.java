package htw.compilerbau.pl0_compiler.lexer;

public enum CharClass {
    ALPHA("ABCDEFGHIJKLMNOPQRSTUVWXYZ"),
    DIGIT("0123456789"),
    COLON(":"),
    EQUAL_SIGN("="),
    SMALLER("<"),
    GREATER(">"),
    SPECIAL("(+-*/(),.;#?!)."),
    SONSTIGE("");

    private String chars = null;

    CharClass(final String chars) {
        this.chars = chars;
    }

    public String getChars() {
        return chars;
    }

    public static CharClass getCharClass(final String oneChar) {
        for (final CharClass charClass : CharClass.values()) {      // TODO: this could be a problem because the order is important?
            if (charClass.getChars().contains(oneChar)) {
                // System.out.println("Matched " + String.valueOf(charClass));
                return charClass;
            }
        }

        // System.out.println("Matched " + String.valueOf(CharClass.NONE));
        return SONSTIGE;

    }

}
