package htw.compilerbau.pl0_compiler.lexer;

public class StateTableCell {
    private String nextState = null;
    private String[] actions = null;

    public StateTableCell(final String nextState, final String actions) {
        this.nextState = nextState;
        final String[] splittedActions = actions.split("\\+");
        if (splittedActions.length <= 1) {
            this.actions = new String[]{actions};
        } else {
            this.actions = splittedActions;
        }

    }

    public String getNextState() {
        return nextState;
    }

    public String[] getActions() {
        return actions;
    }
}
