package htw.compilerbau.pl0_compiler;

import org.junit.Assert;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileUtils {

    private static final Charset DEFAULT_ENCODING = StandardCharsets.UTF_8;

    public static String readFileAsString(final String path) {
        return readFileAsString(path, DEFAULT_ENCODING);
    }

    public static String readFileAsString(final String path, final Charset encoding) {
        try {
            final byte[] encoded = readFileAsBinary(path);
            return new String(encoded, encoding);
        } catch (final NullPointerException e) {
            Assert.fail("The file does not exist: " + path);
            return null;
        }
    }

    public static byte[] readFileAsBinary(final String path) {
        try {
            return Files.readAllBytes(Paths.get(path));
        } catch (final Exception e) {
            Assert.fail("The file does not exist: " + path);
            return null;
        }
    }

    public static Path writeFileAsBinary(final String path, final byte[] bytes) {
        try {
            return Files.write(Paths.get(path), bytes);
        } catch (final Exception e) {
            Assert.fail("The file does not exist: " + path);
            return null;
        }
    }
}
