package htw.compilerbau.pl0_compiler.namensliste;

import htw.compilerbau.pl0_compiler.codegen.Utils;
import htw.compilerbau.pl0_compiler.lexer.Morpheme;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ConstantBlock implements ICanGenerateCode {
    private final List<Constant> constants = new LinkedList<>();
    private final Map<String, Integer> constantNames = new HashMap<>();


    public Constant createConstantWithValue(final Integer number) {

        final Constant constant = new Constant(number, (short) constants.size());
        constants.add(constant);
        return constant;
    }

    public Constant createConstantWithIdentifier(final String identifier) {
        final Constant constant = new Constant(null, (short) constants.size());
        constants.add(constant);
        constantNames.put(identifier, constants.size() - 1);
        return constant;
    }

    public Constant searchConstantByValue(final Morpheme morpheme) {

        for (final Constant constant : constants) {
            if (constant.getValue().equals(morpheme.getValue().getNumber())) {
                return constant;
            }
        }
        return null;
    }

    public Constant searchConstantByIdentifier(final String nameOfConstant) {

        if (constantNames.containsKey(nameOfConstant)) {
            final int constantIndex = constantNames.get(nameOfConstant);
            return constants.get(constantIndex);
        }
        return null;
    }

    @Override
    public String toLittleEndianHexString() {
        String constantBlock = "";

        for (final Constant constant : constants) {
            constantBlock += Utils.toLittleEndianHexString(constant.getValue());
        }

        return constantBlock;
    }

    public Constant getLastCreatedConstant() {
        return constants.get(constants.size() - 1);
    }
}
