package htw.compilerbau.pl0_compiler.namensliste;

public abstract class Identifier {

    private Kennzeichen  kennzeichen = null;		/* Kennzeichen, gibt Typ des Bezeichners an */
    protected String nameOfIdentifier = null;          /* Bezeichner */
    protected Procedure parentProcedure; /* Zeiger auf umgebende Prozedur, null if main */

    public Identifier(Kennzeichen kennzeichen, String nameOfIdentifier, Procedure parentProcedure) {
        this.kennzeichen = kennzeichen;
        this.nameOfIdentifier = nameOfIdentifier;
        this.parentProcedure = parentProcedure;
    }

    public Kennzeichen getKennzeichen() {
        return kennzeichen;
    }

    public String getNameOfIdentifier() {
        return nameOfIdentifier;
    }

    public Procedure getParentProcedure() {
        return parentProcedure;
    }
}
