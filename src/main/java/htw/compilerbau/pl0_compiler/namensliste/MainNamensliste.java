package htw.compilerbau.pl0_compiler.namensliste;

public class MainNamensliste {

    private final Procedure mainProcedure;

    private short currentProcedureIndex = 0; /* Prozedurindex aka ProcedureNummer, increment by 1, start
    once by 0 and
     is continuously incremented */

    public MainNamensliste() {
        mainProcedure = new Procedure(this, null, null, currentProcedureIndex);
    }


    public short getCurrentProcedureIndex() {
        return currentProcedureIndex;
    }

    public Procedure getMainProcedure() {
        return mainProcedure;
    }

    public Short getNextProcedureIndex() {
        currentProcedureIndex++;
        return currentProcedureIndex;
    }

}
