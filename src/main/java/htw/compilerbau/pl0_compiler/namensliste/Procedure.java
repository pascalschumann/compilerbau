package htw.compilerbau.pl0_compiler.namensliste;

import htw.compilerbau.pl0_compiler.codegen.Utils;

import java.util.LinkedList;
import java.util.List;

public class Procedure extends Identifier {

    private final List<Identifier> namensListe = new LinkedList<>();         /* Namensliste     */
    private Short speicherPlatzZuordnungsZaehler = 0;    /* Speicherplatzzuordnungszähler für
    Variablen, increment by 4, starts by 0 for every new procedure */
    private final MainNamensliste mainNamensliste;
    private final Short procedureIndex; /* Prozedurindex aka ProcedureNummer, increment by 1, start
    once by 0 and*/
    private final LabelStack labelStack = new LabelStack();

    private String generatedCode = "";

    public Procedure(final MainNamensliste mainNamensliste, final String nameOfIdentifier,
                     final Procedure parentProcedure, final Short procedureIndex) {
        super(Kennzeichen.PROCEDURE, nameOfIdentifier, parentProcedure);
        this.mainNamensliste = mainNamensliste;
        this.procedureIndex = procedureIndex;
    }

    public Procedure createProcedure(final String nameOfProcedure) {

        final Procedure procedure = new Procedure(mainNamensliste, nameOfProcedure, this,
                mainNamensliste.getNextProcedureIndex());
        namensListe.add(procedure);

        return procedure;
    }

    public Variable createVariable(final String nameOfVariable, final Integer value) {

        final Variable variable = new Variable(nameOfVariable, speicherPlatzZuordnungsZaehler, value,
                this);
        namensListe.add(variable);
        speicherPlatzZuordnungsZaehler = (short) (speicherPlatzZuordnungsZaehler + 4);

        return variable;
    }

    /**
     * @param nameOfVariable that should be searched
     * @return variable if it exists in the nameList of this procedure, else null
     */
    public Variable searchVariableLocally(final String nameOfVariable) {

        return (Variable) searchIdentifierLocally(nameOfVariable, Kennzeichen.VARIABLE);
    }

    /**
     * @param nameOfIdentifier that should be searched
     * @return identifier if it exists in the nameList of this procedure, else null
     */
    public Identifier searchIdentifierLocally(final String nameOfIdentifier, final Kennzeichen kennzeichen) {
        for (final Identifier identifier : namensListe) {
            if (identifier.getKennzeichen().equals(
                    kennzeichen) && identifier.getNameOfIdentifier().equals(
                    nameOfIdentifier)) {
                return identifier;
            }
        }
        return null;
    }

    /**
     * @param nameOfVariable that should be searched from "inside to outside"
     * @return variable if it exists in the nameList of one of the surrounding procedures, else null
     */
    public Variable searchVariableGlobally(final String nameOfVariable) {
        Procedure currentProcdedure = parentProcedure;
        // breaks, if parent is null which means it's the mainProcedure which should Not be searched
        while (currentProcdedure != null) {
            final Variable foundVariable = currentProcdedure.searchVariableLocally(nameOfVariable);
            if (foundVariable != null) {
                return foundVariable;
            }
            currentProcdedure = currentProcdedure.getParentProcedure();
        }
        return null;
    }


    /**
     * @return variable if it exists in any nameList, else null
     */
    public static Procedure searchProcedureGlobally(final String nameOfProcedure, final Procedure mainProcedure) {
        for (final Identifier identifier : mainProcedure.getNamensListe()) {
            if (identifier.getKennzeichen().equals(Kennzeichen.PROCEDURE)) {
                if (identifier.getNameOfIdentifier().equals(
                        nameOfProcedure)) {
                    return (Procedure) identifier;
                } else {
                    final Procedure foundProcedure = searchProcedureGlobally(nameOfProcedure, (Procedure) identifier);
                    if (foundProcedure != null) {
                        return foundProcedure;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public Procedure getParentProcedure() {
        return parentProcedure;
    }

    public short getSpeicherPlatzZuordnungsZaehler() {
        return speicherPlatzZuordnungsZaehler;
    }

    public String getGeneratedCode() {
        return generatedCode;
    }

    public void setGeneratedCode(final String generatedCode) {
        this.generatedCode = generatedCode;
    }

    public MainNamensliste getMainNamensliste() {
        return mainNamensliste;
    }

    public List<Identifier> getNamensListe() {
        return namensListe;
    }

    public Variable searchVariableMain(final String nameOfVariable) {
        return mainNamensliste.getMainProcedure().searchVariableLocally(nameOfVariable);
    }


    public Variable searchVariable(final String nameOfVariable) {
        // search in main first !!!
        Variable variable = searchVariableMain(nameOfVariable);
        if (variable != null) {
            return variable;
        }
        variable = searchVariableLocally(nameOfVariable);
        if (variable != null) {
            return variable;
        }

        variable = searchVariableGlobally(nameOfVariable);
        return variable;
    }

    /**
     * Bezeichner gefunden 	   |Code	            |Parameter
     * ---------------------------------------------------------------
     * Local                   |PushValVarLocal     |Relativadresse
     * Im Hauptprogramm        |PushValVarMain      |Relativadresse
     * In umgebender Prozedur  |PushValVarGlobal    |Relativadresse, Prozedurnummer
     */
    public String generatePushValueOfVariable(final String nameOfVariable) {

        // search in main first !!!
        Variable variable = searchVariableMain(nameOfVariable);
        if (variable != null) {
            return "01" + Utils.toLittleEndianHexString(variable.getRelativAdresse());
        }
        variable = searchVariableLocally(nameOfVariable);
        if (variable != null) {
            return "00" + Utils.toLittleEndianHexString(variable.getRelativAdresse());
        }

        variable = searchVariableGlobally(nameOfVariable);
        return "02" + Utils.toLittleEndianHexString(variable.getRelativAdresse()) + Utils.toLittleEndianHexString(variable.getParentProcedure().getProcedureIndex());
    }

    /**
     * Bezeichner gefunden	    Code	            Parameter
     * -----------------------------------------------------------
     * Local	                PushAdrVarLocal	    Relativadresse
     * Im Hauptprogramm	        PushAdrVarMain	    Relativadresse
     * In umgebender Prozedur	PushAdrVarGlobal	Relativadresse, Prozedurnummer
     *
     * @return generated code
     */
    public String generatePushAddressOfVariable(final String nameOfVariable) {

        // search in main first !!!
        Variable variable = searchVariableMain(nameOfVariable);
        if (variable != null) {
            return "04" + Utils.toLittleEndianHexString(variable.getRelativAdresse());
        }
        variable = searchVariableLocally(nameOfVariable);
        if (variable != null) {
            return "03" + Utils.toLittleEndianHexString(variable.getRelativAdresse());
        }
        variable = searchVariableGlobally(nameOfVariable);
        return "05" + Utils.toLittleEndianHexString(variable.getRelativAdresse()) + Utils.toLittleEndianHexString(variable.getParentProcedure().getProcedureIndex());
    }

    /**
     * @return procedureIndex aka procedureNumber
     */
    public Short getProcedureIndex() {
        return procedureIndex;
    }

    public Variable getLastCreatedVariable() {
        if (namensListe.size() == 1) {
            if (namensListe.get(0).equals(Kennzeichen.VARIABLE)) {
                return (Variable) namensListe.get(0);
            } else {
                return null;
            }
        }
        for (int i = namensListe.size() - 1; i >= 0; i--) {
            if (namensListe.get(i).getKennzeichen().equals(Kennzeichen.VARIABLE)) {
                return (Variable) namensListe.get(i);
            }
        }
        return null;
    }

    public LabelStack getLabelStack() {
        return labelStack;
    }
}
