package htw.compilerbau.pl0_compiler.namensliste;

public interface ICanGenerateCode {

    String toLittleEndianHexString();
}
