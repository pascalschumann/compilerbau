package htw.compilerbau.pl0_compiler.namensliste;

/**
 * Label saves the position of jnot in generated code of procedure
 *
 * @author Pascal Schumann (pascal.schumann@sap.com)
 */
public class Label {

    private final Integer positionInGeneratedCode;
    private final Procedure parentProcedure;

    public Label(Integer positionInGeneratedCode, Procedure parentProcedure) {
        this.positionInGeneratedCode = positionInGeneratedCode;
        this.parentProcedure = parentProcedure;
    }

    public Integer getPositionInGeneratedCode() {
        return positionInGeneratedCode;
    }

    public Procedure getParentProcedure() {
        return parentProcedure;
    }
}
