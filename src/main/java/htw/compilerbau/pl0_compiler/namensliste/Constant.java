package htw.compilerbau.pl0_compiler.namensliste;

import htw.compilerbau.pl0_compiler.codegen.Utils;

public class Constant {


    private Integer value;		/* Wert der Konstanten, 4 Byte <*/
    private Short  indexKonstantenblock;		/* Index im Konstantenblock, 2 Byte*/

    public Constant(Integer value, Short indexKonstantenblock) {
        this.value = value;
        this.indexKonstantenblock = indexKonstantenblock;
    }

    public Integer getValue() {
        return value;
    }

    public Short getIndexKonstantenblock() {
        return indexKonstantenblock;
    }

    public String generate(){
        return  "06" + Utils.toLittleEndianHexString(
                indexKonstantenblock);
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
