package htw.compilerbau.pl0_compiler.namensliste;

public class Variable extends Identifier {

    private Short relativAdresse;	/* Relativadresse der Variablen, aka displacement */
    private Integer value;		/* Wert der Variable, 4 Byte <*/


    public Variable(String nameOfIdentifier, Short relativAdresse, Integer value,
                    Procedure parentProcedure) {
        super(Kennzeichen.VARIABLE, nameOfIdentifier, parentProcedure);
        this.relativAdresse = relativAdresse;
        this.value = value;
    }

    /**
     * @return relativAdresse aka displacement
     */
    public Short getRelativAdresse() {
        return relativAdresse;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
