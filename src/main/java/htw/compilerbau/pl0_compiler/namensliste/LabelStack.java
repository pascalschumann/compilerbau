package htw.compilerbau.pl0_compiler.namensliste;

import java.util.Stack;

public class LabelStack {
    private final Stack<Label> labels = new Stack<>();

    public Label pop() {
        return labels.pop();
    }

    public void push(final Integer positionInGeneratedCode, final Procedure parentProcedure) {
        labels.push(new Label(positionInGeneratedCode, parentProcedure));
    }
}
