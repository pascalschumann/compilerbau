
import htw.compilerbau.pl0_compiler.FileUtils;
import htw.compilerbau.pl0_compiler.codegen.Utils;
import org.junit.Assert;
import org.junit.Test;

/**
 * Currently this class only converts given hexcode to binary files in order to test them with the vm.
 */
public class TestPl0Extensions {

    final String PREFIX_PATH = "src/test/resources/extensions/";

    String[] filesToConvert = new String[]{
            "else1", "else0", "for"
    };

    public void testForLoop() {
        final String example = "";
    }

    @Test
    public void testConvert() {

        for (final String fileToConvert : filesToConvert) {
            final String cl0Text = fileToConvert + ".cl0.txt";
            final String cl0Binary = fileToConvert + ".cl0";
            String source = FileUtils.readFileAsString(PREFIX_PATH + cl0Text);
            Assert.assertNotEquals("Source should not be empty.", 0, source.length());
            source = source.replace(" ", "");
            source = source.replace("\n", "");
            final byte[] bytes = Utils.hexStringToByteArray2(source);
            FileUtils.writeFileAsBinary(PREFIX_PATH + cl0Binary, bytes);
        }
    }
}
