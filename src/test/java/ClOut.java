import htw.compilerbau.pl0_compiler.FileUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ClOut {

    private final String clOut;

    public ClOut(final String clOutFileName) {
        clOut = FileUtils.readFileAsString(clOutFileName);
    }

    public List<String> clOutAsLines(final String clOut) {
        final List<String> lines = new LinkedList<>();
        final String[] originalLines = clOut.split("\n");
        // ignore first line
        for (int i = 1; i < originalLines.length; i++) {
            final String originalLine = originalLines[i];
            String line = "";
            if (!originalLine.contains(":")) {
                continue;
            }
            final String[] splittedLine = originalLine.split("\\s+");
            line += splittedLine[1];
            if (splittedLine.length > 3) {
                line += splittedLine[3];
            }
            lines.add(line);
        }
        return lines;
    }

    public String formatClOut(final List<String> clOutLines) {
        String formatted = "";
        for (final String line : clOutLines) {
            formatted += line.replace(",", "") + "\n";
        }
        return formatted;
    }

    public String formatClOut() {
        final List<String> clOutAsLines = clOutAsLines(clOut);
        return formatClOut(clOutAsLines);
    }

    public String formatHexBinaryStringLikeFormattedClOutString(String generatedCode) {
        final List<String> expectedClOutAsLines = clOutAsLines(clOut);

        generatedCode = generatedCode.substring(8);
        final List<String> clOutLinesGeneratedCode = new LinkedList<>();
        for (final String clOutToken : expectedClOutAsLines) {
            String realClOutToken = clOutToken;

            if (realClOutToken.contains(",")) {
                realClOutToken = realClOutToken.replace(",", "");
            }
            if (generatedCode.length() < realClOutToken.length()) {
                break;
            }
            clOutLinesGeneratedCode.add(generatedCode.substring(0, realClOutToken.length()));
            generatedCode = generatedCode.substring(realClOutToken.length());
        }
        return formatClOut(clOutLinesToBigEndian(clOutLinesGeneratedCode));
    }

    public List<String> clOutLinesToBigEndian(final List<String> clOutLinesLittleEndian) {
        final List<String> bigEndianLines = new LinkedList<>();
        for (final String clOutLine : clOutLinesLittleEndian) {
            String newLine = clOutLine.substring(0, 2); // command
            if (clOutLine.length() > 2) {
                final int sizeOfArg = 4; // short has 4 chars
                final String[] args = splitToNChar(clOutLine.substring(2), sizeOfArg);
                for (int i = 0; i < args.length; i++) {
                    // swap the bytes
                    newLine += args[i].substring(2);
                    newLine += args[i].substring(0, 2);
                }
            }
            bigEndianLines.add(newLine);
        }
        return bigEndianLines;
    }

    /**
     * Split text into n number of characters.
     *
     * @param text the text to be split.
     * @param size the split size.
     * @return an array of the split text.
     */
    private static String[] splitToNChar(final String text, final int size) {
        final List<String> parts = new ArrayList<>();

        final int length = text.length();
        for (int i = 0; i < length; i += size) {
            parts.add(text.substring(i, Math.min(length, i + size)));
        }
        return parts.toArray(new String[0]);
    }
}
