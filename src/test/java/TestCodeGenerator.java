import htw.compilerbau.pl0_compiler.FileUtils;
import htw.compilerbau.pl0_compiler.codegen.Compiler;
import htw.compilerbau.pl0_compiler.lexer.Morpheme;
import htw.compilerbau.pl0_compiler.parser.Parser;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class TestCodeGenerator {

    private static final String PREFIX_PATH = "src/test/resources/";
    private static final String[] testFilesCodeGen = new String[]{"tmin", "tmin2", "tmin3",
            "tmin4", "tmin5", "tmin6", "tmin7", "tmin9", "tm", "tmin10", "tmin11", "abgabe",
            "test_files2/fakultRecursiv", "test_files2/t2",
            "test_files2/t5", "test_files2/t6", "test_files2/tm", "test_files2/tmin", "test_files2/tx"
    };

    @Test
    public void testParserWithTestFiles() {

        for (final String testFile : testFilesCodeGen) {
            testParserWithTestFilesHelper(testFile);
        }
    }

    private void testParserWithTestFilesHelper(final String testFile) {
        System.out.println("Starting with file: " + testFile);

        final String source = FileUtils.readFileAsString(PREFIX_PATH + testFile + ".pl0");
        Assert.assertNotEquals("Source should not be empty.", 0, source.length());

        final Parser parser = new Parser();
        final List<Morpheme> restTokens = parser.parse(source);
        System.out.println("leftover tokens: " + restTokens);
        Assert.assertTrue("Program could not be parsed: " + testFile, restTokens.isEmpty());
        System.out.println("----------------------------");
        System.out.println("Successfully parsed: " + testFile);
    }

    @Test
    public void testCodeGenerationWithTestFiles() {

        for (final String testFile : testFilesCodeGen) {

            testCodeGenerationWithTestFilesHelper(testFile);
        }
    }

    private void testCodeGenerationWithTestFilesHelper(final String testFile) {
        System.out.println("Generating code for: " + testFile);
        final String source = FileUtils.readFileAsString(PREFIX_PATH + testFile + ".pl0");
        Assert.assertNotEquals("Source should not be empty.", 0, source.length());

        final String expectedGeneratedCode = FileUtils.readFileAsString(
                PREFIX_PATH + testFile + ".cl0" + ".hex.txt").trim();
        Assert.assertNotEquals("ExpectedGeneratedCode should not be empty", 0,
                expectedGeneratedCode.length());

        // for debug purposes
        final ClOut expectedClOut = new ClOut(PREFIX_PATH + testFile + ".cl0.txt");
        final String expectedClOutFormatted = expectedClOut.formatClOut();

        final Compiler compiler = new Compiler();
        final String actualGeneratedCode = compiler.generateCode(source);
        Assert.assertFalse("Generated code should not be empty.",
                actualGeneratedCode.isEmpty());
        final String actualClOutFormatted =
                expectedClOut.formatHexBinaryStringLikeFormattedClOutString(
                        actualGeneratedCode);

        Assert.assertEquals(
                "Generated code for " + testFile + " is not correct:\n" + expectedClOutFormatted + "\n" + actualClOutFormatted,
                expectedGeneratedCode.toUpperCase(), actualGeneratedCode.toUpperCase());

        System.out.println("Successfully generated code for: " + testFile);
    }

}
